import schema from './schema';
import rootValue from './root';

export {
  schema,
  rootValue,
};
