import { PeopleModel } from '../models/People';

const getPeoples = async (args: any) => {
  const result = await PeopleModel.findOne({ name: args.name }).exec();

  if (!result) throw new Error('Not found');

  return await PeopleModel.aggregate([
    {
      $match: {
        $and: [
          {
            _id: { $ne: result._id }
          },
          {
            $or: [
              { species: { $elemMatch: { $in: result.species } } },
              { birth_year: { $lte: result.birth_year + 100, $gte: result.birth_year - 100, $ne: 0 } },
              { homeworld: result.homeworld },
              { eye_color: result.eye_color },
              { height: { $lte: result.height + 100, $gte: result.height - 100, $ne: 0 } },
              { mass: { $lte: result.mass + 100, $gte: result.mass - 100, $ne: 0 } },
            ],
          },
        ],
      },
    },
    {
      $project: {
        _id: '$_id',
        name: '$name',
        species: '$species',
        birth_year: '$birth_year',
        homeworld: '$homeworld',
        eye_color: '$eye_color',
        height: '$height',
        mass: '$mass',
        total: {
          $add: [
            {
              $cond: {
                if: {
                  $and: [
                    { $gte: [ '$birth_year', result.birth_year - 100 ] },
                    { $lte: [ '$birth_year', result.birth_year + 100 ] },
                    { $ne: [ '$birth_year', 0 ] },
                  ],
                }, then: { $multiply: [ { $divide: [ { $subtract: [100, { $abs: { $subtract: ['$birth_year', result.birth_year] } } ] }, 100 ] }, 5 ] } , else: 0,
              },
            },
            {
              $cond: {
                if: {
                  $ne: [
                    {
                      $size: {
                        $filter: {
                          input: '$species',
                          as: 'x',
                          cond: { $in: ['$$x', result.species] },
                        },
                      },
                    },
                    0,
                  ],
                },
                then: 30,
                else: 0,
              },
            },
            { $cond: { if: { $eq: [ '$homeworld', result.homeworld ] }, then: 5, else: 0 } },
            { $cond: { if: { $eq: [ '$eye_color', result.eye_color ] }, then: 20, else: 0 } },
            {
              $cond: {
                if: {
                  $and: [
                    { $gte: [ '$height', result.height - 100 ] },
                    { $lte: [ '$height', result.height + 100 ] },
                    { $ne: [ '$height', 0 ] },
                  ]
                }, then: { $multiply: [ { $divide: [ { $subtract: [ 100, { $abs: { $subtract: ['$height', result.height] } } ] }, 100 ] }, 17 ] } , else: 0,
              },
            },
            {
              $cond: {
                if: {
                  $and: [
                    { $gte: [ '$mass', result.mass - 100 ] },
                    { $lte: [ '$mass', result.mass + 100 ] },
                    { $ne: [ '$mass', 0 ] },
                  ],
                }, then: { $multiply: [ { $divide: [ { $subtract: [ 100, { $abs: { $subtract: [ '$mass', result.mass ] } } ] }, 100 ] }, 23 ] } , else: 0,
              },
            },
          ],
        },
      },
    },
    {
      $sort: {
        'total': -1,
      },
    },
    {
      $limit: 5,
    },
  ]).exec();
};

const autocomplete = async (args: any) => {
  const result = await PeopleModel
    .find({ name: new RegExp(`^${args.name}`, 'i') })
    .limit(10)
    .exec();

  return result;
};

const rootValue = {
  peoples: getPeoples,
  autocomplete,
};

export default rootValue;