import { buildSchema } from 'graphql';

const schema = buildSchema(`
    type Query {
        peoples(name: String): [People]
        autocomplete(name: String): [People]
    }

    type People {
        id: String
        name: String
        birth_year: Float
        eye_color: String
        height: Int
        weight: Int
        species: [String]
        homeworld: String
    }
`);

export default schema;