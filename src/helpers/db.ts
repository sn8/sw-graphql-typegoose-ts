import mongoose from 'mongoose';
import { name } from '../../package.json';

const connectDb = async (dropDatabase: boolean = false) => {
  const mongoUri = process.env.MONGODB_URI || `mongodb://localhost:27017/${name}`;
  const conn = await mongoose.connect(mongoUri, { useNewUrlParser: true });
  if (dropDatabase) conn.connection.db.dropDatabase();
};

export default connectDb;