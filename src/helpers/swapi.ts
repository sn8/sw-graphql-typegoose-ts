import axios from 'axios';

const swapiBase = 'https://swapi.co/api';

const getData = (url: string): Promise<Array<{}>> => {
  return new Promise(async (resolve) => {
    let data: Array<{}> = [];
    while (url != null) {
      try {
        const res = await axios.get(url);
        data = [...data, ...res.data.results];
        url = res.data.next;
      } catch(err) {
        console.log(`Failed to get data from "${url}". Let's try again...`);
      }
    }
    resolve(data);
  });
}

export const getPlanets = (): Promise<Array<{}>> => {
  return getData(`${swapiBase}/planets`);
};

export const getSpecies = (): Promise<Array<{}>> => {
  return getData(`${swapiBase}/species`);
};

export const getPeoples = (): Promise<Array<{}>> => {
  return getData(`${swapiBase}/people`);
};
