import { PeopleModel, SpeciesModel, PlanetModel } from '../models';
import { getPlanets, getSpecies, getPeoples } from './swapi';

const populateCollection = (Model: any, data: Array<{}>): Promise<any> => {
  const promises = data.map((item) => {
    return new Promise(async (resolve) => {
      const toSave = new Model(item);
      await toSave.save();
      resolve();
    });
  });

  return Promise.all(promises);
};

const populateDb = async () => {
  console.log('populateDb is starting now...');

  const [species, planets, peoples] = await Promise.all([
    getSpecies(),
    getPlanets(),
    getPeoples(),
  ]);

  await Promise.all([
    populateCollection(SpeciesModel, species),
    populateCollection(PlanetModel, planets),
  ]);

  await populateCollection(PeopleModel, peoples);

  console.log('populateDb is done!');
};

export default populateDb;
