import express from 'express';
import graphqlHTTP from 'express-graphql';
import cors from 'cors';
import connectDb from './helpers/db';
import populateDb from './helpers/populate-db';
import { schema, rootValue } from './graphql';

const port = process.env.PORT || 3000;
const app = express();

connectDb(true);
populateDb();

app.use(cors());
app.use('/', graphqlHTTP({
  schema,
  rootValue,
  graphiql: true,
}));

app.listen(port);
console.log(`GraphQL API server running at localhost:${port}`);
