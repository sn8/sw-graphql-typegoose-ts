import { prop, Typegoose } from 'typegoose';

export class Planet extends Typegoose {
  @prop({ required: true })
  url: string;

  @prop({ required: true })
  name: string;

  @prop()
  climate?: string;

  @prop()
  diameter?: string | number;

  @prop()
  gravity?: string | number;

  @prop()
  orbital_period?: string | number;

  @prop()
  population?: string | number;

  @prop()
  surface_water?: string | number;

  @prop()
  terrain?: string;

  @prop()
  created?: string;

  @prop()
  edited?: string;

  @prop()
  residents?: Array<string>;

  @prop()
  films?: Array<string>;
}

export const PlanetModel = new Planet().getModelForClass(Planet);
