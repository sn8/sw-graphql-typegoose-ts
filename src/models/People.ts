import { prop, Typegoose, pre } from 'typegoose';
import { PlanetModel, SpeciesModel } from '.';

@pre<People>('save', async function(next) {
  if (this.isModified('birth_year')) {
    const { birth_year } = this;
    const match = birth_year.match(/^(\d.{0,10})(A|B)BY/);

    if (match) {
      this.birth_year = /BBY/.test(birth_year)
        ? -Number(match[1])
        : Number(match[1]);
    } else {
      this.birth_year = null;
    }
  }

  const numOrNull = (value: any) => /\d/.test(value) ? Number(value) : null;
  if (this.isModified('height')) this.height = numOrNull(this.height);
  if (this.isModified('weight')) this.weight = numOrNull(this.weight);
  if (this.isModified('mass')) this.mass = numOrNull(this.mass);

  if (this.isModified('homeworld')) {
    const result = await PlanetModel
      .findOne({ url: this.homeworld })
      .exec();
    if (result) this.homeworld = result.name;
  }

  if (this.isModified('species')) {
    const promises = this.species.map((url) => {
      return SpeciesModel.findOne({ url }).exec();
    });

    const results = await Promise.all(promises);
    this.species = results.map(result => result.name);
  }

  next();
})

export class People extends Typegoose {
  @prop({ required: true })
  url: string;

  @prop({ required: true })
  name: string;

  @prop()
  gender?: string;

  @prop()
  birth_year?: any;

  @prop()
  skin_color?: string;

  @prop()
  hair_color?: string;

  @prop()
  eye_color?: string;

  @prop()
  mass?: any;

  @prop()
  height?: any;

  @prop()
  weight?: any;

  @prop()
  created?: string;

  @prop()
  edited?: string;

  @prop()
  species?: Array<string>;

  @prop()
  homeworld?: string;

  @prop()
  films?: Array<string>;

  @prop()
  vehicles?: Array<string>;

  @prop()
  starships?: Array<string>;
};

export const PeopleModel = new People().getModelForClass(People);
