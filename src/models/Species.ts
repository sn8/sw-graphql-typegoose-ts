import { prop, Typegoose } from 'typegoose';

export class Species extends Typegoose {
  @prop({ required: true })
  url: string;

  @prop({ required: true })
  name: string;

  @prop()
  average_height?: string | number;

  @prop()
  average_lifespan?: string | number;

  @prop()
  classification?: string;

  @prop()
  designation?: string;

  @prop()
  eye_colors?: string;

  @prop()
  hair_colors?: string;

  @prop()
  language?: string;

  @prop()
  skin_colors?: string;

  @prop()
  created?: string;

  @prop()
  edited?: string;

  @prop()
  people?: Array<string>;

  @prop()
  films?: Array<string>;
}

export const SpeciesModel = new Species().getModelForClass(Species);