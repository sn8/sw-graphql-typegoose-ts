import { PeopleModel } from './People';
import { PlanetModel } from './Planet';
import { SpeciesModel } from './Species';

export {
  PeopleModel,
  PlanetModel,
  SpeciesModel,
};
